package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedTicketException;

import java.util.*;

public class ParkingLot {

    private Integer capacity = 10;

    private Map<ParkingTicket, Car> carTickets = new HashMap<>();

    public ParkingLot() {

    }

    public ParkingLot(Integer capacity) {
        this.capacity = capacity;
    }

    public Collection<Car> getCars() {
        return this.carTickets.values();
    }

    public boolean isFull() {
        return carTickets.size() == capacity;
    }

    public boolean isContain(ParkingTicket parkingTicket) {
        return carTickets.containsKey(parkingTicket);
    }

    public Integer getRemainingSize() {
        return this.capacity - carTickets.size();
    }

    public Double getAvailablePositionRate() {
        return (double) getRemainingSize() / capacity;
    }

    public ParkingTicket park(Car car) {
        if(carTickets.size() == capacity) {
            throw  new NoAvailablePositionException();
        }
        ParkingTicket parkingTicket = new ParkingTicket();
        carTickets.put(parkingTicket, car);
        return parkingTicket;
    }

    public Car fetch(ParkingTicket parkingTicket) {
        if(!carTickets.containsKey(parkingTicket)) {
            throw new UnrecognizedTicketException();
        }
        return carTickets.remove(parkingTicket);
    }
}
