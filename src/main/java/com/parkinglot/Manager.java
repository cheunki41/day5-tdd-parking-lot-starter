package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedTicketException;

import java.util.ArrayList;
import java.util.List;

public class Manager extends ParkingBoy{

    private List<ParkingBoy> managementList = new ArrayList<>();

    public Manager() {
        super(new ArrayList<>());
    }

    public void addParkingBoy(ParkingBoy parkingBoy) {
        managementList.add(parkingBoy);
    }

    public List<ParkingBoy> getManagementList() {
        return managementList;
    }

    public ParkingTicket specificPark(Car car) {
        return managementList.stream()
                .filter(parkingBoy -> parkingBoy.parkingLots.stream()
                        .anyMatch(parkingLot -> !parkingLot.isFull()))
                .findFirst()
                .orElseThrow(NoAvailablePositionException::new)
                .park(car);
    }

    public Car specificFetch(ParkingTicket parkingTicket) {
        return managementList.stream()
                .filter(parkingBoy -> parkingBoy.parkingLots.stream()
                        .anyMatch(parkingLot -> parkingLot.isContain(parkingTicket)))
                .findFirst()
                .orElseThrow(UnrecognizedTicketException::new)
                .fetch(parkingTicket);
    }
}
