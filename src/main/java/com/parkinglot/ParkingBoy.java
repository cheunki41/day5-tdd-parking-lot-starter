package com.parkinglot;

import com.parkinglot.exception.UnrecognizedTicketException;
import com.parkinglot.strategy.SmartParkingStrategy;
import com.parkinglot.strategy.StandardParkingStrategy;
import com.parkinglot.strategy.SuperParkingStrategy;

import java.util.List;
import java.util.Optional;

public class ParkingBoy {

    protected List<ParkingLot> parkingLots;
    private ParkingBoyType parkingBoyType = ParkingBoyType.STANDARD;

    public ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public ParkingBoy(List<ParkingLot> parkingLots, ParkingBoyType parkingBoyType) {
        this.parkingLots = parkingLots;
        this.parkingBoyType = parkingBoyType;
    }

    public ParkingTicket park(Car car) {
        switch(parkingBoyType) {
            case STANDARD:
                return new StandardParkingStrategy().park(car, parkingLots);
            case SMART:
                return new SmartParkingStrategy().park(car, parkingLots);
            case SUPER:
                return new SuperParkingStrategy().park(car, parkingLots);
            default:
                return null;
        }
    }

    public Car fetch(ParkingTicket parkingTicket) {
        Optional<ParkingLot> validParkingLot = parkingLots.stream()
                .filter(parkingLot -> parkingLot.isContain(parkingTicket))
                .findFirst();

        if(validParkingLot.isPresent()) {
            return validParkingLot.get().fetch(parkingTicket);
        }

        throw new UnrecognizedTicketException();
    }
}
