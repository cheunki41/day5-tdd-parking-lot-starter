package com.parkinglot.strategy;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;
import com.parkinglot.exception.NoAvailablePositionException;

import java.util.List;
import java.util.Optional;

public class StandardParkingStrategy implements ParkingStrategy {
    @Override
    public ParkingTicket park(Car car, List<ParkingLot> parkingLots) {
        Optional<ParkingLot> availableParkingLot = parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .findFirst();

        if(availableParkingLot.isPresent()) {
            return availableParkingLot.get().park(car);
        }

        throw new NoAvailablePositionException();
    }
}
