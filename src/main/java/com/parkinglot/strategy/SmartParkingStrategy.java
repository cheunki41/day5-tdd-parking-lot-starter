package com.parkinglot.strategy;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;

import java.util.Comparator;
import java.util.List;

public class SmartParkingStrategy implements ParkingStrategy{
    @Override
    public ParkingTicket park(Car car, List<ParkingLot> parkingLots) {
        return parkingLots.stream()
                .max(Comparator.comparing(parkingLot -> parkingLot.getRemainingSize()))
                .get().park(car);
    }
}
