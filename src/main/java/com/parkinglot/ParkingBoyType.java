package com.parkinglot;

public enum ParkingBoyType {
    STANDARD,
    SMART,
    SUPER
}
