package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedTicketException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingBoyTest {

    @Test
    public void should_park_at_first_parking_lot_when_park_given_parking_boy_and_car() {
        // given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = parkingBoy.park(car);

        // then
        assertNotNull(parkingTicket);
        assertTrue(firstParkingLot.getCars().contains(car));
    }

    @Test
    public void should_park_at_second_parking_lot_when_park_given_parking_boy_and_car() {
        // given
        ParkingLot firstParkingLot = new ParkingLot();
        IntStream.range(0, 10)
                .forEach(number -> firstParkingLot.park(new Car()));
        ParkingLot secondParkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = parkingBoy.park(car);

        // then
        assertNotNull(parkingTicket);
        assertTrue(secondParkingLot.getCars().contains(car));
    }

    @Test
    public void should_return_car_when_fetch_given_parking_boy_and_parking_ticket() {
        // given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot));
        Car car = new Car();
        ParkingTicket parkingTicket = parkingBoy.park(car);

        // when
        Car actual = parkingBoy.fetch(parkingTicket);

        // then
        assertEquals(car, actual);
    }

    @Test
    public void should_return_right_car_when_fetch_given_parking_boy_and_two_parking_tickets() {
        // given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        Car firstCar = new Car();
        Car secondCar = new Car();
        ParkingTicket firstParkingTicket = firstParkingLot.park(firstCar);
        ParkingTicket secondParkingTicket = secondParkingLot.park(secondCar);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot));

        // when
        Car actualFirstCar = parkingBoy.fetch(firstParkingTicket);
        Car actualSecondCar = parkingBoy.fetch(secondParkingTicket);

        // then
        assertEquals(firstCar, actualFirstCar);
        assertEquals(secondCar, actualSecondCar);
    }

    @Test
    public void should_throw_exception_when_park_given_two_full_parking_lots_and_car_and_parking_boy() {
        // given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        IntStream.range(0, 10)
                .forEach(number -> {
                    firstParkingLot.park(new Car());
                    secondParkingLot.park(new Car());
                });
        ParkingBoy parkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot));

        // when
        Executable parking = () -> parkingBoy.park(new Car());

        // then
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, parking);
        assertEquals("No available position.", exception.getMessage());
    }

    @Test
    public void should_throw_exception_when_fetch_given_wrong_ticket() {
        // given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot));
        ParkingTicket incorrectTicket = new ParkingTicket();

        // when
        Executable fetching = () -> parkingBoy.fetch(incorrectTicket);

        // then
        UnrecognizedTicketException exception = assertThrows(UnrecognizedTicketException.class, fetching);
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    public void should_throw_exception_when_fetch_given_used_ticket() {
        // given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot));
        ParkingTicket parkingTicket = parkingBoy.park(new Car());
        parkingBoy.fetch(parkingTicket);

        // when
        Executable fetching = () -> parkingBoy.fetch(parkingTicket);

        // then
        UnrecognizedTicketException exception = assertThrows(UnrecognizedTicketException.class, fetching);
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }


}
