package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedTicketException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingLotTest {
    @Test
    public void should_return_parkingTicket_when_park_given_parkingLot_and_car() {
        // given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = parkingLot.park(car);

        // then
        assertNotNull(parkingTicket);
    }

    @Test
    public void should_return_car_when_fetch_given_parkingLot_and_parkingTicket() {
        // given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car);

        // when
        Car returnCar = parkingLot.fetch(parkingTicket);

        // then
        assertEquals(car, returnCar);
    }

    @Test
    public void should_return_the_right_car_when_fetch_given_parkingLot_and_two_parkingTickets() {
        // given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);

        // when
        Car returnCar1 = parkingLot.fetch(parkingTicket1);
        Car returnCar2 = parkingLot.fetch(parkingTicket2);

        // then
        assertEquals(returnCar1, car1);
        assertEquals(returnCar2, car2);
    }

    @Test
    public void should_return_unrecognized_parking_ticket_when_fetch_given_parkingLot_and_wrong_parkingTicket() {
        // given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        parkingLot.park(car1);
        ParkingTicket wrongTicket = new ParkingTicket();

        // when
        Executable fetching = () -> parkingLot.fetch(wrongTicket);

        // then
        String exceptionMessage = assertThrows(UnrecognizedTicketException.class, fetching).getMessage();
        assertEquals("Unrecognized parking ticket.", exceptionMessage);
    }

    @Test
    public void should_throw_unrecognized_parking_ticket_when_fetch_given_parking_lot_and_used_parkingTicket() {
        // given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car1);
        parkingLot.fetch(parkingTicket);

        // when
        Executable fetching = () -> parkingLot.fetch(parkingTicket);

        // then
        String exceptionMessage = assertThrows(UnrecognizedTicketException.class, fetching).getMessage();
        assertEquals("Unrecognized parking ticket.", exceptionMessage);
    }

    @Test
    public void should_throw_no_available_position_exception_when_park_given_full_parking_lot_and_car() {
        // given
        ParkingLot parkingLot = new ParkingLot();
        IntStream.range(0 ,10)
                        .forEach(number -> parkingLot.park(new Car()));

        // when
        Executable parking =  () -> parkingLot.park(new Car());

        // then
        String exceptionMessage = assertThrows(NoAvailablePositionException.class, parking).getMessage();
        assertEquals("No available position.", exceptionMessage);
    }
}
