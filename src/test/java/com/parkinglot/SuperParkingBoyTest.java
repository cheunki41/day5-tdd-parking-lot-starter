package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.List;

import static com.parkinglot.ParkingBoyType.SUPER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SuperParkingBoyTest {

    @Test
    public void should_park_at_first_parking_lot_when_park_given_super_parking_boy_and_car() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(5);
        ParkingLot secondParkingLot = new ParkingLot();
        ParkingBoy superParkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot), SUPER);

        secondParkingLot.park(new Car());
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = superParkingBoy.park(car);

        // then
        assertEquals(car, firstParkingLot.fetch(parkingTicket));
    }

    @Test
    public void should_park_at_second_parking_lot_when_park_given_super_parking_boy_and_car() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(5);
        ParkingLot secondParkingLot = new ParkingLot(10);
        ParkingBoy superParkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot), SUPER);
        firstParkingLot.park(new Car());
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = superParkingBoy.park(car);

        // then
        assertEquals(car, secondParkingLot.fetch(parkingTicket));
    }

    @Test
    public void should_park_at_first_parking_lot_when_park_given_super_parking_boy_and_car_and_two_same_available_position_rate_parking_lot() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(5);
        ParkingLot secondParkingLot = new ParkingLot(5);
        firstParkingLot.park(new Car());
        secondParkingLot.park(new Car());
        ParkingBoy superParkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot), SUPER);
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = superParkingBoy.park(car);

        // then
        assertEquals(car, firstParkingLot.fetch(parkingTicket));
    }

    @Test
    public void should_throw_exception_when_park_given_super_parking_boy_and_car_and_two_full_parking_lots() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        firstParkingLot.park(new Car());
        secondParkingLot.park(new Car());
        ParkingBoy superParkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot), SUPER);
        Car car = new Car();

        // when
        Executable parking = () -> superParkingBoy.park(car);

        // then
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, parking);
        assertEquals("No available position.", exception.getMessage());
    }

}
