package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedTicketException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ManagerTest {

    @Test
    public void should_add_parking_boy_to_list_when_addParkingBoy_given_manager_and_parkingBoy() {
        // given
        ParkingBoy parkingBoy = new ParkingBoy(new ArrayList<>());
        Manager manager = new Manager();

        // when
        manager.addParkingBoy(parkingBoy);

        // then
        assertEquals(1, manager.getManagementList().size());
    }

    @Test
    public void should_park_by_parking_boy_when_specificPark_given_manager_and_parking_boy() {
        // given
        ParkingBoy parkingBoy = new ParkingBoy(List.of(new ParkingLot()));
        Manager manager = new Manager();
        manager.addParkingBoy(parkingBoy);
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = manager.specificPark(car);

        // then
        assertEquals(car, parkingBoy.fetch(parkingTicket));
    }

    @Test
    public void should_park_by_second_parking_boy_when_specificPark_given_manager_and_two_parking_boys() {
        // given
        ParkingLot parkingLot = new ParkingLot(1);
        parkingLot.park(new Car());
        ParkingBoy firstParkingBoy = new ParkingBoy(List.of(parkingLot));
        ParkingBoy secondParkingBoy = new ParkingBoy(List.of(new ParkingLot()));
        Manager manager = new Manager();
        manager.addParkingBoy(firstParkingBoy);
        manager.addParkingBoy(secondParkingBoy);
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = manager.specificPark(car);

        // then
        assertEquals(car, secondParkingBoy.fetch(parkingTicket));
    }

    @Test
    public void should_fetch_by_parking_boy_when_specificFetch_given_manager_and_parking_boy() {
        // given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot));
        Manager manager = new Manager();
        manager.addParkingBoy(parkingBoy);

        // when
        Car actual = manager.specificFetch(parkingTicket);

        // then
        assertEquals(car, actual);
    }

    @Test
    public void should_fetch_by_correct_parking_boy_when_specificFetch_given_manager_and_two_parking_tickets() {
        // given
        ParkingLot firstParkingLot = new ParkingLot();
        Car firstCar = new Car();
        ParkingTicket firstParkingTicket = firstParkingLot.park(firstCar);
        ParkingBoy firstParkingBoy = new ParkingBoy(List.of(firstParkingLot));
        ParkingLot secondParkingLot = new ParkingLot();
        Car secondCar = new Car();
        ParkingTicket secondParkingTicket = secondParkingLot.park(secondCar);
        ParkingBoy secondParkingBoy = new ParkingBoy(List.of(secondParkingLot));
        Manager manager = new Manager();
        manager.addParkingBoy(firstParkingBoy);
        manager.addParkingBoy(secondParkingBoy);

        // when
        // then
        assertEquals(firstCar, manager.specificFetch(firstParkingTicket));
        assertEquals(secondCar, manager.specificFetch(secondParkingTicket));
    }

    @Test
    public void should_throw_exception_when_specificPark_given_manager_and_parking_boy_with_full_parking_lot() {
        // given
        ParkingLot parkingLot = new ParkingLot(1);
        parkingLot.park(new Car());
        Manager manager = new Manager();
        manager.addParkingBoy(new ParkingBoy(List.of(parkingLot)));

        // when
        Executable parking = () -> manager.specificPark(new Car());

        // then
        String exceptionMessage = assertThrows(NoAvailablePositionException.class, parking).getMessage();
        assertEquals("No available position.", exceptionMessage);
    }

    @Test
    public void should_throw_exception_when_specificFetch_given_manager_and_wrong_ticket() {
        // given
        Manager manager = new Manager();
        manager.addParkingBoy(new ParkingBoy(List.of(new ParkingLot())));

        // when
        Executable fetching = () -> manager.specificFetch(new ParkingTicket());

        // then
        String exceptionMessage = assertThrows(UnrecognizedTicketException.class, fetching).getMessage();
        assertEquals("Unrecognized parking ticket.", exceptionMessage);
    }

    @Test
    public void should_throw_exception_when_specificFetch_given_manager_and_used_ticket() {
        // given
        Manager manager = new Manager();
        ParkingBoy parkingBoy = new ParkingBoy(List.of(new ParkingLot()));
        ParkingTicket parkingTicket = parkingBoy.park(new Car());
        parkingBoy.fetch(parkingTicket);
        manager.addParkingBoy(parkingBoy);

        // when
        Executable fetching = () -> manager.specificFetch(parkingTicket);

        // then
        String exceptionMessage = assertThrows(UnrecognizedTicketException.class, fetching).getMessage();
        assertEquals("Unrecognized parking ticket.", exceptionMessage);
    }

}
