package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartParkingBoyTest {

    @Test
    public void should_park_at_first_parking_lot_when_park_given_smart_parking_boy_and_car() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(10);
        ParkingLot secondParkingLot = new ParkingLot(5);
        ParkingBoy smartParkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot), ParkingBoyType.SMART);
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = smartParkingBoy.park(car);

        // then
        assertEquals(car, firstParkingLot.fetch(parkingTicket));
    }


    @Test
    public void should_park_at_second_parking_lot_when_park_given_smart_parking_boy_and_car() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(5);
        ParkingLot secondParkingLot = new ParkingLot(10);
        ParkingBoy smartParkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot), ParkingBoyType.SMART);
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = smartParkingBoy.park(car);

        // then
        assertEquals(car, secondParkingLot.fetch(parkingTicket));
    }

    @Test
    public void should_park_at_first_parking_lot_when_park_given_smart_parking_boy_and_car_and_two_same_capacity_parking_lot() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(5);
        ParkingLot secondParkingLot = new ParkingLot(5);
        ParkingBoy smartParkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot), ParkingBoyType.SMART);
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = smartParkingBoy.park(car);

        // then
        assertEquals(car, firstParkingLot.fetch(parkingTicket));
    }

    @Test
    public void should_throw_exception_when_park_given_smart_parking_boy_and_two_full_parking_lot() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(0);
        ParkingLot secondParkingLot = new ParkingLot(0);
        ParkingBoy smartParkingBoy = new ParkingBoy(List.of(firstParkingLot, secondParkingLot), ParkingBoyType.SMART);
        Car car = new Car();

        // when
        Executable parking = () -> smartParkingBoy.park(car);

        // then
        String exceptionMessage = assertThrows(NoAvailablePositionException.class, parking).getMessage();
        assertEquals("No available position.", exceptionMessage);
    }
}
